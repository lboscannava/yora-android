package com.isis.yora.activities;

import android.os.Bundle;

import com.isis.yora.R;
import com.isis.yora.views.MainNavDrawer;

public class SentMessagesActivity extends BaseAuthenticatedActivity {

    @Override
    protected void onYoraCreate(Bundle savedState) {
        setContentView(R.layout.activity_sent_messages);
        setNavDrawer(new MainNavDrawer(this));
    }
}
